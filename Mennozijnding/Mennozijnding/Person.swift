//
//  Person.swift
//  Mennozijnding
//
//  Created by Shopboostr  on 02/03/16.
//  Copyright © 2016 Jari Vdberg. All rights reserved.
//

import UIKit

class Person: NSObject {
    var name: String
    var image: String
    
    init(name: String, image: String) {
        self.name = name
        self.image = image
    }
}