//
//  PersonCell.swift
//  Mennozijnding
//
//  Created by Shopboostr  on 02/03/16.
//  Copyright © 2016 Jari Vdberg. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
}
